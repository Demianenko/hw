package com.company.java;

public class Main {

    public static void main(String[] args) {
    Books book = new Books("Harry Potter", "fantasy", 100);
    Magazines vogue = new Magazines("Vogue",12,15 );
    Shop myShop = new Shop();
    myShop.addBook(book);
    myShop.removeBook(book);
    myShop.addMagazines(vogue);
    myShop.removeMagazines(vogue);
    }
}
