package com.company.java;

import java.util.ArrayList;

public class Shop {

    ArrayList<Books> booksList = new ArrayList();
    ArrayList<Magazines> magazinesList = new ArrayList();

    public void addBook(Books book) {
        booksList.add(book);
    }

    public void removeBook(Books book) {
        if (!booksList.contains(book)) {
            System.out.println("Book is not found");
        } else {
            booksList.remove(book);
        }
    }

    public void addMagazines(Magazines vogue) {
        magazinesList.add(vogue);
    }

    public void removeMagazines(Magazines vogue) {
        if (!magazinesList.contains(vogue)) {
            System.out.println("Magazine is not found");
        } else {
            magazinesList.remove(vogue);

        }
    }
}

